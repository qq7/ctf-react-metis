// auto-generated from https://chainid.network/chains.json
export const networksMetaInfo: { [chainId: string]: any } = {
  59902: {
    name: 'Metis Sepolia',
    currency: 'tMetis',
    explorer: 'https://sepolia-explorer.metisdevops.link'
  },
  1337: { name: 'Local Hardhat' },
  31337: { name: 'Local Hardhat' }
}
